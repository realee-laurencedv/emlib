# Silabs mcu lib code

This repo contain peripheral and mcu code from [Silabs Gecko SDK][silabsdoc_link] please see included original [readme][origreadme_file]
Contact [maintainer][maintainer_email] for information and/or questions

## Disclaimer

We are not the original author of this code, Silabs is the [owner/maintainer][silabsdoc_link] of the original source code.  
This repo exist only to have git access to this code only, all files are copied from the _sdk folder_ of [SS][ss_link].  
This is only needed because there is no official git repo maintained containing this code.

## Install

1. Include the `inc` folder in your search path
2. Add the `src` folder to your compiler path

[origreadme_file]:  ReadMe_emlib.txt

[silabsdoc_link]:   https://docs.silabs.com/
[ss_link]:          https://www.silabs.com/products/development-tools/software/simplicity-studio
[maintainer_email]: <MAILTO:laurencedv@realee.tech>
